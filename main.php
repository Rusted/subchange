<?php

$outFile = 'output.srt';

if (isset($argv[1])) {
    $fileName = $argv[1];
} else {
    die('use first parameter as a file name, e.g. "input.srt"');
}

if (isset($argv[2])) {
    $addSecondThousands = $argv[2];
} else {
    die('use second parameter as second thousands to speed up the subs, e.g. "1500"');
}

if (isset($argv[3])) {
    $outFile = $argv[3];
}



$infile = fopen("input.srt", "r") or die("Unable to open file for reading!");
$outfile = fopen("output.srt", "w") or die("Unable to open file for writing!");

 while (($line = fgets($infile)) !== false) {
     $parts = explode(' --> ', $line);
     foreach ( $parts as $key => &$part) {
         $pattern = '/([0-9]{2}):([0-9]{2}):([0-9]{2}),([0-9]{3})/i';
         $res = preg_match($pattern, $part, $matches, PREG_OFFSET_CAPTURE);
         if ($res) {
             $hours = (int) $matches[1][0];
             $minutes = (int) $matches[2][0];
             $seconds = (int) $matches[3][0];
             $secondThousands =  (int) $matches[4][0];

             $secondThousandsTotal =
                 $secondThousands
                 + $seconds * 1000
                 + $minutes * 60 * 1000
                 + $hours * 60 * 60 * 1000
             ;

             $secondThousandsTotal = $secondThousandsTotal + $addSecondThousands;
             $hours = intdiv($secondThousandsTotal, (60 * 60 * 1000));
             $minutes = intdiv(($secondThousandsTotal - $hours * (60 * 60 * 1000)),(60 * 1000));
             $seconds = intdiv(($secondThousandsTotal - $hours  * (60 * 60 * 1000) - $minutes * (60 * 1000)), 1000);
             $secondThousands = ($secondThousandsTotal - $hours * (60 * 60 * 1000) - $minutes * (60 * 1000)- $seconds * 1000);

             $hours = sprintf('%02d', $hours);
             $minutes = sprintf('%02d', $minutes);
             $seconds = sprintf('%02d', $seconds);

             $part = $hours . ':' . $minutes . ':' . $seconds . ',' . $secondThousands;
         }

         fwrite($outfile, $part);
         if (count($parts) > 1 && $key == 0) {
             fwrite($outfile, ' --> ');
         }

         if (count($parts) > 1 && $key == 1) {
             fwrite($outfile, PHP_EOL);
         }
     }
}

echo 'Completed.';

fclose($infile);
fclose($outfile);